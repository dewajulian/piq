package email;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/")
public class utility {
	
	public utility()
	{

	}
	
	@GET
	@Path("sendemail/{param}")
	public Response sendEmail(@PathParam("param") String msg) {
 
		String output = "email sent : " + msg;
 
		return Response.status(200).entity(output).build();
 
	}

}