package service;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/")
public class registration {

	@GET
	@Path("newtenant/{param}")
	public Response newTenant(@PathParam("param") String msg) {
 
		String output = "New Tenant : " + msg;
 
		return Response.status(200).entity(output).build();
 
	}
}